export type TUser = {
  id: string;
  avatar: string;
  name: string;
  createdAt: string;
};
