export type TOrder = {
  id: string;
  userId: string;
  createdAt: string;
  total: string;
  currency: string;
  city: string;
};
