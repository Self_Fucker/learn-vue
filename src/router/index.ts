import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      // При таком подключении компонент будет загружен сразу(даже если он ещё не показывается пользователю)
      component: Home,
    },
    {
      path: '/users',
      name: 'users',
      // сплиттинг кода по уровням маршрутеризации
      // Такое подключение в замыкании даст сборщику понять, что этот компонент надо разбить в отдельный файл (Например Users.[hash].js))
      // Такой компонент будет загружаться только тогда, когда пользователь перейдёт по этому роуту, называется lazy loading
      component: () => import('../views/Users.vue'),
    },
    {
      path: '/user/:id',
      name: 'user',
      component: () => import('../views/User.vue'),
    },
    {
      path: '/orders',
      name: 'orders',
      component: () => import('../views/Orders.vue'),
    },
  ],
});

export default router;
