import { acceptHMRUpdate, defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const useCounterStore = defineStore('counter', () => {
  const counter = ref<number>(0);
  const doubleCount = computed(() => counter.value * 2);
  function increment() {
    counter.value++;
  }

  return { counter, doubleCount, increment };
});

// Hot Module Replacement
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCounterStore, import.meta.hot));
}
